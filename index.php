<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Albert Chu</title>
	<!-- STYLES: BOOTSTRAPS -->
	<link type="text/css" href="http://students.cec.wustl.edu/~albert.chu/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="http://students.cec.wustl.edu/~albert.chu/css/bootstrap-responsive.min.css" rel="stylesheet">
	<!-- STYLES: THEMES -->
	<link type="text/css" href="http://students.cec.wustl.edu/~albert.chu/css/theme.css" rel="stylesheet">
	<link type="text/css" href="http://students.cec.wustl.edu/~albert.chu/css/theme-responsive-767.css" rel="stylesheet">
	<link type="text/css" href="http://students.cec.wustl.edu/~albert.chu/css/theme-responsive-768-979.css" rel="stylesheet">
	<!-- ICONS -->

	<link type="text/css" href="http://students.cec.wustl.edu/~albert.chu/css/style.css" rel="stylesheet">
	<link rel="shortcut icon" href="http://students.cec.wustl.edu/~albert.chu/icons/favicon.png" type="image/png">
	<script type="text/javascript" src="http://students.cec.wustl.edu/~albert.chu/js/jquery-2.0.3.min.js"></script>
	<script>
$(document).ready(function(){
  $('a[href="' + window.location.hash + '"]').click()
});
	</script>

<body>

	<div class="container">
		<div class="row">
			<div class="sidebar span5">
				<div class="cover">
					<div class="cover-photo">
						<div class="cover-image">
							<a class="cover-avatar">
								<img src="http://students.cec.wustl.edu/~albert.chu/icons/user.png" alt="Image of Albert Chu">
							</a>
						</div>
					</div>
					<div class="cover-author">
						<h1 class="brand">Albert Chu</h1>
						<h2 class="tagline">Computer Science Student at Washington University in St. Louis</h2>

					</div>
				</div>
			</div>

			<div class="content span7 offset5">
				<ul class="nav">
					<li class="active"><a href="#home">
						<i class="icon-home shaded"></i>
						<b>Home</b>
					</a></li>
					<li><a href="#profile">
						<i class="icon-briefcase shaded"></i>
						<b>Resume/Contact Me</b>
					</a></li>

				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="home">
						<section class="about">
							<h2 class="section-head"><b></b>
								About
							</h2>

							<p>I am a Computer Science student at <a href="http://colleges.usnews.rankingsandreviews.com/best-colleges/washington-university-in-st.-louis-2520">
								Washington University in St. Louis</a> scheduled to graduate May 2015.  Originally, I am from Ardsley, New York.</p>
						</section>


						<section class="experience">
							<h2 class="section-head"><b></b>Experience</h2>

							<ul class="list-media unstyled">
								<li>
									<div class="media">
										<img class="media-avatar pull-left" src="http://students.cec.wustl.edu/~albert.chu/icons/icon6.png" alt = "YouTube Logo">
										<div class="media-body">
											<h3 class="company">YouTube</h3>
											<p class="role">Software Engineer<span class="muted">
												(Starting in August, 2015)</span></p>
											<p class="brief">
												- Signed offer to work on the YouTube team of Google after graduation.
											</p>
										</div>
									</div>
								</li>

								<li>
									<div class="media">
										<img class="media-avatar pull-left" src="http://students.cec.wustl.edu/~albert.chu/icons/icon5.png" alt = "Yelp Logo">
										<div class="media-body">
											<h3 class="company">Yelp</h3>
											<p class="role">Software Engineering Intern<span class="muted">
												(May - August, 2014)</span></p>
											<p class="brief">
												- Worked on the "Your Next Review Awaits" homepage widget.
											</p>
										</div>
									</div>
								</li>

								<li>
									<div class="media">
										<img class="media-avatar pull-left" src="http://students.cec.wustl.edu/~albert.chu/icons/icon1.png" alt = "MasterCard Logo">
										<div class="media-body">
											<h3 class="company">MasterCard Worldwide</h3>
											<p class="role">Software Development Intern<span class="muted">
												(June - August, 2013)</span></p>
											<p class="brief">
												- Built and Deployed a 3-Tiered Security Architecture involving an Apache HTTP Server, a JBoss Java Application Server, and a PostgreSQL Database Server (all running on Red Hat Enterprise Linux)  These servers were created within an on-premise cloud environment, powered by VMWare.
											</p>
											<p class="brief">
												- Contributed to monitoring application that retrieves information about virtual machines in MasterCard’s cloud environment
											</p>
											<p class="brief">
												- Wrote web application in Python, HTML, and JavaScript that checks if internal bridge lines are currently being used, and updates a database correspondingly
											</p>
											<p class="brief">
												- Assisted in Technical Resolution Team mitigation procedure enhancements for high priority issues
											</p>
										</div>
									</div>
								</li>

								<li>
									<div class="media">
										<img class="media-avatar pull-left" src="http://students.cec.wustl.edu/~albert.chu/icons/icon2.png" alt = "Washington University Logo">
										<div class="media-body">
											<h3 class="company">Washington University School Of Engineering and Applied Science</h3>
											<p class="role">Teaching Assistant for CSE 260 (Introduction to Digital Logic and Computer Design) <span class="muted">(January - May, 2013)</span></p>
											<p class="brief">
												- Reviewed and evaluated the work of students
											</p>
											<p class="brief">
												- Aided students in the understanding of course material
											</p>
										</div>
									</div>
								</li>
								<li>
									<div class="media">
										<img class="media-avatar pull-left" src="http://students.cec.wustl.edu/~albert.chu/icons/icon3.png" alt = "Verizon Logo">
										<div class="media-body">
											<h3 class="company">Verizon Communications</h3>
											<p class="role">Network Operations Intern <span class="muted">(June - August, 2012)</span></p>
											<p class="brief">
												- Designed internal web pages using HTML, JavaScript, and ColdFusion
											</p>
											<p class="brief">
												- Created Visio diagrams of Verizon's networks
											</p>
											<p class="brief">
												- Composed templates for potential future Verizon website redesigns
											</p>
											<p class="brief">
												- Monitored the health and status of Verizon networks
											</p>

										</div>
									</div>
								</li>
							</ul>
						</section>

						<section class="sideprojects">
							<h2 class="section-head"><b></b>Side Projects</h2>

							<ul class="list-media unstyled">
								<li>
									<div class="media">
										<img class="media-avatar pull-left" src="http://students.cec.wustl.edu/~albert.chu/icons/icon7.png" alt = "Convex Hull Applet Logo">
										<div class="media-body">
											<h3 class="company">Convex Hull Applet</h3>

											<p class="brief">
												Created a Java applet that allows the user to create a set of points, and run various different convex hull algorithms on the points.
												The algorithms (Graham's Scan, Jarvis' March, Quick Hull, and Merge Hull) are shown step by step.
											</p>
											<p class="brief">
												<a href="applet"> Applet available here. </a>
											<p>

										</div>
									</div>
								</li>

							</ul>

							<ul class="list-media unstyled">
								<li>
									<div class="media">
										<img class="media-avatar pull-left" src="http://students.cec.wustl.edu/~albert.chu/icons/icon4.png" alt = "Robo-Writer Logo">
										<div class="media-body">
											<h3 class="company">Robo-Writer</h3>

											<p class="brief">
												Collaborated with team of 4 to write system for transcribing voice into text and auto-filling web forms with the text.
												This web application was built to be used for the automation of Red Cross Operators creating incident reports from phone calls.
												In order to determine the correct placement of items into proper web forms, we utilized natural language processing algorithms.
												The Tropo API was used for the transcription of text.  With Robo-Writer, we won the Everyone Hacks Chicago hack-a-thon, which was themed
												around urban disaster applications.
											</p>

										</div>
									</div>
								</li>

							</ul>
						</section>

						<section class="technicalskills">
							<h2 class="section-head"><b></b>Technical Skills</h2>

							<ul class="list-media unstyled">
								<li>
									<div class="media">
										<div class="media-body">
											<p class="brief">
												- Python, Java, C++, JavaScript, C, HTML, CSS, Shell Scripting, VHDL
											</p>
											<p class="brief">
												- Microsoft Windows, Mac OS X, Linux
											</p>
											<p class="brief">
												- Microsoft Office
											</p>

										</div>
									</div>
								</li>

							</ul>
						</section>

						<section class="education">
							<h2 class="section-head"><b></b>Education</h2>


							<dl class="timeline dl-horizontal">
								<dt>2011 - 2015</dt>
								<dd>
									<strong>Bachelor of Science in Computer Science</strong>
									<p>Washington University in St. Louis</p>
								</dd>
								<dt>2007 - 2011</dt>
								<dd>
									<strong>Diploma</strong>
									<p>Ardsley High School</p>
								</dd>
							</dl>
						</section>

					</div><!--/.tab-pane-->

					<div class="tab-pane" id="profile">
						<section class="Resume">
							<h2 class="section-head"><b></b>Resume</h2>
							<p><a href="AlbertChuResume.pdf">For a pdf copy of my resume, click here.</a></p>


						</section>

						<section class="contact-details">
						<h2 class="section-head"><b></b>Contact Me</h2>
						<p>If you have any questions or job inquiries, you can contact me with the form below.</p>

						<?php

        // check for a successful form post
        if (isset($_GET['s'])) echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$_GET['s']."</div>";

        // check for a form error
        elseif (isset($_GET['e'])) echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>".$_GET['e']."</div>";

				?>

						<hr>
						<form method="POST" action="contact-form-submission.php">
							<ul class="unstyled">
								<li class="row-fluid">
									<p class="span6"><input type="text" id="input1" name="contact_name" placeholder="Your name" class="span12"></p>
									<p class="span6"><input type="text" id="input2" name="contact_email" placeholder="Your email" class="span12"></p>
								</li>
								<li class="row-fluid">
									<p class="span12"><textarea id="input3" name="contact_message" placeholder="Your message" class="span12" rows="8"></textarea></p>
								</li>
								<li>
									<p class="span6">
										<input type="hidden" name="save" value="contact">
                <button type="submit" class="btn btn-primary">Send</button>
									</p>
								</li>
							</ul>
						</form>
					</section>
					</div><!--/.tab-pane-->





				</div><!--/.tab-content-->
			</div>
		</div>
	</div>


	<!-- SCRIPTS: BASE -->
	<script src="http://students.cec.wustl.edu/~albert.chu/js/jquery-1.9.1.min.js"></script>
	<script src="http://students.cec.wustl.edu/~albert.chu/js/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="http://students.cec.wustl.edu/~albert.chu/js/bootstrap.min(1).js"></script>

	<script>
	$('.nav a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})
	</script>
</body></html>